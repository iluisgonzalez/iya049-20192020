const List = ({ head, tail }) => ({
    head,
    tail,
  });

List.prototype.getType = () => {
    return "List";
}

List.prototype.addToTail = (itemNext) => {
    tail = itemNext;
}
List.prototype.addToHead = (itemHead) => {
    head = itemHead;
}
List.prototype.head = () => {
    return head;
}
List.prototype.isEmpty = () => {
    if(head == null && tail == null){
        return true;
    }
    return false;
}
List.prototype.map = (fn) => {
    Object.entries(this).forEach(element1 => {
        //console.log(element1); 
        element1.forEach(element2 => {
          //console.log(element2) 
          if(element2 != "head" && element2 != "tail"){
            //console.log(element2); 
            if(typeof(element2) != "object"){
              console.log(fn(element2));
            }else if(typeof(element2) == "object"){
              console.log(fn(element2.head));
            }/*else if(typeof(element2) == "undefined"){
              console.log("nil");   
            }*/
          }
        });
      }); 
}

module.exports(List);
