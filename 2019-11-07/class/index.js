const List = class { 
    constructor(head, tail) {
     
        this.head = null;
        this.tail = null;
        this.addToHead(head);
        this.addToTail(tail);
      
    }
    
    isEmpty(){
        if(this.head == null && this.tail == null){
            return true;
        }
        return false;
    }
    
    head(){
        return this.head;
    }

    addToTail(itemNext){
        this.tail = itemNext;
    }

    addToHead(itemHead){
        this.head = itemHead;
    }

   map(fn){
    console.log(this);
    return console.log(fn(this.head)),console.log(fn(this.tail.head));
   /* Object.entries(this).forEach(element1 => {
      //console.log(element1); 
      element1.forEach(element2 => {
        //console.log(element2) 
        if(element2 != "head" && element2 != "tail"){
          //console.log(element2); 
          if(typeof(element2) != "object"){
            console.log(fn(element2));
          }else if(typeof(element2) == "object"){
            console.log(fn(element2.head));
          }/*else if(typeof(element2) == "undefined"){
            console.log("nil");   
          }
        }
      });
    }); */
     

    }

    getType(){
        return console.log("List");
    }
    
} 

const Nil = class { 

  constructor(){

  }

  getType(){
    return "nil";
  }
}

module.exports = {List, Nil};

const food = new List("broccoli", new List("kale", new List()));

console.log(food.head); // -> broccoli
console.log(food.map(meal => `steamed ${meal}`)); // -> ("steamed broccoli", ("steamed kale", nil))
//console.log(food.map(food));
console.log(food.getType()); // -> List
console.log(new List().getType()); // -> List
