const createList = ({ head, tail }) => ({
    head,
    tail,
    addToHead (head) {
      this.head = head;
    },

    addToTail (tail){
        this.tail = tail;
    },

    isEmpty(){
      if(this.head == null && this.tail == null){
        return true;
      }
    return false;
    },

    head(){
      return this.head;
    },

    getType(){
      return console.log("List");
    },

    map(fn){
    
      Object.entries(this).forEach(element1 => {
        //console.log(element1); 
        element1.forEach(element2 => {
          //console.log(element2) 
          if(element2 != "head" && element2 != "tail"){
            //console.log(element2); 
            if(typeof(element2) != "object"){
              console.log(fn(element2));
            }else if(typeof(element2) == "object"){
              console.log(fn(element2.head));
            }/*else if(typeof(element2) == "undefined"){
              console.log("nil");   
            }*/
          }

        });
      }); 
  
    }

  });

  module.exports(createList);