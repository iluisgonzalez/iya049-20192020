const url = require('url');

var Coins = 0;
var Lifes = 3;
exports.getlifeCoinsRequest = function (req, res) {
    const reqUrl = url.parse(req.url, true);

    if (reqUrl.query.name) {
        name = reqUrl.query.name
    }

    var response = {
        "Coins": Coins,
        "Lifes": Lifes
    };

    res.writeHead(200, {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*"
      });
    res.end(JSON.stringify(response));
};

exports.setCoinsRequest = function (req, res) {
    body = '';

    req.on('data', function (chunk) {
        body += chunk;
        //console.log("this is the body", body);

    });

    req.on('end', function () {

        postBody = JSON.parse(body);
        Coins = parseInt(postBody.coins);

        var response = {
            "text": "Post Request coins is  " + postBody.coins,
        };

        
        res.writeHead(200, {
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin": "*"
          });
        res.end(JSON.stringify(response));
    });
};

exports.setLifesRequest = function (req, res) {
    body = '';

    req.on('data', function (chunk) {
        body += chunk;
        
        //console.log("this is the body", body);

    });

    req.on('end', function () {

        postBody = JSON.parse(body);
        Lifes = parseInt(postBody.lifes);

        var response = {
            "text2": "Post Request lifes is  " + postBody.lifes
        };

        
        res.writeHead(200, {
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin": "*"
          });
        res.end(JSON.stringify(response));
    });
};

exports.invalidRequest = function (req, res) {
    res.statusCode = 404;
    res.setHeader('Content-Type', 'text/plain');
    res.end('Invalid Request');
};