(() => {
  const newStream5 = ticks5.map(() => (Math.random() <= 0.5 ? RIGHTe : LEFTe));

  // X MOVEMENT
  const leftKeyDownse5 = newStream5.filter(x5 => x5 === -1).map(() => LEFTe);
  const rightKeyDownse5 = newStream5.filter(x5 => x5 === 1).map(() => RIGHTe);

  const movementse5 = Stream.merge(leftKeyDownse5, rightKeyDownse5);

  const initialPositione5 = Stream.of(randomIntFromInterval(460, 570));
  const positionIncrementse5 = movementse5.map(
    directione5 => directione5 * VELOCITY_X_ENEMY
  );

  const wombatPos5 = Stream.merge(initialPositione5, positionIncrementse5).scan(
    (currentPositione5, incremente5) => currentPositione5 + incremente5,
    0
  );

  wombatPos5.subscribe(xm5 => {
    if (xm5 > 450 && xm5 < 580) {
      WOMBAT_DIV5.style.left = `${xm5}px`;
      const collision1 = makeContact(MARIO_DIV, WOMBAT_DIV1);
      const collision2 = makeContact(MARIO_DIV, WOMBAT_DIV2);
      const collision3 = makeContact(MARIO_DIV, WOMBAT_DIV3);
      const collision4 = makeContact(MARIO_DIV, WOMBAT_DIV4);
      const collision5 = makeContact(MARIO_DIV, WOMBAT_DIV5);
      const collision6 = makeContact(MARIO_DIV, WOMBAT_DIV6);

      if (collision5) {
        wombatsCollision(
          collision1,
          collision2,
          collision3,
          collision4,
          collision5,
          collision6
        );
      }
    }
  });
  // X MOVEMENT

  function randomIntFromInterval(min, max) {
    // min and max included
    return Math.floor(Math.random() * (max - min + 1) + min);
  }
})();
