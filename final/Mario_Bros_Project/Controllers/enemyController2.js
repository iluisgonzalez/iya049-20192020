(() => {
  const newStream2 = ticks2.map(() => (Math.random() <= 0.5 ? RIGHTe : LEFTe));

  // X MOVEMENT
  const leftKeyDownse2 = newStream2.filter(x2 => x2 === -1).map(() => LEFTe);
  const rightKeyDownse2 = newStream2.filter(x2 => x2 === 1).map(() => RIGHTe);

  const movementse2 = Stream.merge(leftKeyDownse2, rightKeyDownse2);

  const initialPositione2 = Stream.of(randomIntFromInterval(350, 550));
  const positionIncrementse2 = movementse2.map(
    directione2 => directione2 * VELOCITY_X_ENEMY
  );

  const wombatPos2 = Stream.merge(initialPositione2, positionIncrementse2).scan(
    (currentPositione2, incremente2) => currentPositione2 + incremente2,
    0
  );

  wombatPos2.subscribe(xm2 => {
    if (xm2 > 60 && xm2 < 570) {
      WOMBAT_DIV2.style.left = `${xm2}px`;
      const collision1 = makeContact(MARIO_DIV, WOMBAT_DIV1);
      const collision2 = makeContact(MARIO_DIV, WOMBAT_DIV2);
      const collision3 = makeContact(MARIO_DIV, WOMBAT_DIV3);
      const collision4 = makeContact(MARIO_DIV, WOMBAT_DIV4);
      const collision5 = makeContact(MARIO_DIV, WOMBAT_DIV5);
      const collision6 = makeContact(MARIO_DIV, WOMBAT_DIV6);

      if (collision2) {
        wombatsCollision(
          collision1,
          collision2,
          collision3,
          collision4,
          collision5,
          collision6
        );
      }
    }
  });
  // X MOVEMENT

  function randomIntFromInterval(min, max) {
    // min and max included
    return Math.floor(Math.random() * (max - min + 1) + min);
  }
})();
