(() => {
  const isLeft = event => "ArrowLeft" === event.code;
  const isRight = event => "ArrowRight" === event.code;
  const isUp = event => "ArrowUp" === event.code;
  const isDown = event => "ArrowDown" === event.code;

  // X MOVEMENT
  const leftKeyDowns = keyDowns.filter(isLeft).map(() => LEFT);
  const rightKeyDowns = keyDowns.filter(isRight).map(() => RIGHT);

  const movements = Stream.merge(leftKeyDowns, rightKeyDowns);

  const initialPosition = Stream.of(280);
  const positionIncrements = movements.map(
    direction => direction * VELOCITY_PLAYER
  );

  const pcPosition = Stream.merge(initialPosition, positionIncrements).scan(
    (currentPosition, increment) => currentPosition + increment,
    0
  );

  pcPosition.subscribe(x => {
    if (x > 0 && x < 660) {
      MARIO_DIV.style.left = `${x}px`;

      const collisionCoin1 = makeContact(MARIO_DIV, COIN_DIV1);
      const collisionCoin2 = makeContact(MARIO_DIV, COIN_DIV2);
      const collisionCoin3 = makeContact(MARIO_DIV, COIN_DIV3);
      const collisionCoin4 = makeContact(MARIO_DIV, COIN_DIV4);

      CoinsCollision(
        collisionCoin1,
        collisionCoin2,
        collisionCoin3,
        collisionCoin4
      );
    }
  });
  // X MOVEMENTs

  // Y MOVEMENT
  const upKeyDowns = keyDowns.filter(isUp).map(() => UP);
  const downKeyDowns = keyDowns.filter(isDown).map(() => DOWN);

  const movementinY = Stream.merge(upKeyDowns, downKeyDowns);

  const initialPosInY = Stream.of(335);
  const posIncrementInY = movementinY.map(
    directionY => directionY * VELOCITYINY_PLAYER
  );

  const marioPositioninY = Stream.merge(initialPosInY, posIncrementInY).scan(
    (actualPosinY, incrementInY) => actualPosinY + incrementInY,
    0
  );

  marioPositioninY.subscribe(y => {
    if (y > 0 && y < 480) {
      MARIO_DIV.style.top = `${y}px`;

      const collisionCoin1 = makeContact(MARIO_DIV, COIN_DIV1);
      const collisionCoin2 = makeContact(MARIO_DIV, COIN_DIV2);
      const collisionCoin3 = makeContact(MARIO_DIV, COIN_DIV3);
      const collisionCoin4 = makeContact(MARIO_DIV, COIN_DIV4);

      CoinsCollision(
        collisionCoin1,
        collisionCoin2,
        collisionCoin3,
        collisionCoin4
      );
    }
  });
  // Y MOVEMENT
})();
