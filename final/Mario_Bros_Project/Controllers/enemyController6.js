(() => {
  const newStream6 = ticks6.map(() => (Math.random() <= 0.5 ? RIGHTe : LEFTe));

  // X MOVEMENT
  const leftKeyDownse6 = newStream6.filter(x6 => x6 === -1).map(() => LEFTe);
  const rightKeyDownse6 = newStream6.filter(x6 => x6 === 1).map(() => RIGHTe);

  const movementse6 = Stream.merge(leftKeyDownse6, rightKeyDownse6);

  const initialPositione6 = Stream.of(randomIntFromInterval(250, 400));
  const positionIncrementse6 = movementse6.map(
    directione6 => directione6 * VELOCITY_X_ENEMY
  );

  const wombatPos6 = Stream.merge(initialPositione6, positionIncrementse6).scan(
    (currentPositione6, incremente6) => currentPositione6 + incremente6,
    0
  );

  wombatPos6.subscribe(xm6 => {
    if (xm6 > 240 && xm6 < 410) {
      WOMBAT_DIV6.style.left = `${xm6}px`;
      const collision1 = makeContact(MARIO_DIV, WOMBAT_DIV1);
      const collision2 = makeContact(MARIO_DIV, WOMBAT_DIV2);
      const collision3 = makeContact(MARIO_DIV, WOMBAT_DIV3);
      const collision4 = makeContact(MARIO_DIV, WOMBAT_DIV4);
      const collision5 = makeContact(MARIO_DIV, WOMBAT_DIV5);
      const collision6 = makeContact(MARIO_DIV, WOMBAT_DIV6);

      if (collision6) {
        wombatsCollision(
          collision1,
          collision2,
          collision3,
          collision4,
          collision5,
          collision6
        );
      }
    }
  });
  // X MOVEMENT

  function randomIntFromInterval(min, max) {
    // min and max included
    return Math.floor(Math.random() * (max - min + 1) + min);
  }
})();
