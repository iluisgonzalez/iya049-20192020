(() => {
  const newStream = ticks.map(() => (Math.random() <= 0.5 ? RIGHTe : LEFTe));

  // X MOVEMENT
  const leftKeyDownse = newStream.filter(x => x === -1).map(() => LEFTe);
  const rightKeyDownse = newStream.filter(x => x === 1).map(() => RIGHTe);

  const movementse = Stream.merge(leftKeyDownse, rightKeyDownse);

  const initialPositione = Stream.of(randomIntFromInterval(100, 250));
  const positionIncrementse = movementse.map(
    directione => directione * VELOCITY_X_ENEMY
  );

  const wombatPos = Stream.merge(initialPositione, positionIncrementse).scan(
    (currentPositione, incremente) => currentPositione + incremente,
    0
  );

  wombatPos.subscribe(x => {
    if (x > 60 && x < 570) {
      WOMBAT_DIV1.style.left = `${x}px`;
      const collision1 = makeContact(MARIO_DIV, WOMBAT_DIV1);
      const collision2 = makeContact(MARIO_DIV, WOMBAT_DIV2);
      const collision3 = makeContact(MARIO_DIV, WOMBAT_DIV3);
      const collision4 = makeContact(MARIO_DIV, WOMBAT_DIV4);
      const collision5 = makeContact(MARIO_DIV, WOMBAT_DIV5);
      const collision6 = makeContact(MARIO_DIV, WOMBAT_DIV6);

      if (collision1) {
        wombatsCollision(
          collision1,
          collision2,
          collision3,
          collision4,
          collision5,
          collision6
        );
      }
    }
  });
  // X MOVEMENT

  function randomIntFromInterval(min, max) {
    // min and max included
    return Math.floor(Math.random() * (max - min + 1) + min);
  }
})();
