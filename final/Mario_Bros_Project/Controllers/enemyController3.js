(() => {
  const newStream3 = ticks3.map(() => (Math.random() <= 0.5 ? RIGHTe : LEFTe));

  // X MOVEMENT
  const leftKeyDownse3 = newStream3.filter(x3 => x3 === -1).map(() => LEFTe);
  const rightKeyDownse3 = newStream3.filter(x3 => x3 === 1).map(() => RIGHTe);

  const movementse3 = Stream.merge(leftKeyDownse3, rightKeyDownse3);

  const initialPositione3 = Stream.of(randomIntFromInterval(250, 380));
  const positionIncrementse3 = movementse3.map(
    directione3 => directione3 * VELOCITY_X_ENEMY
  );

  const wombatPos3 = Stream.merge(initialPositione3, positionIncrementse3).scan(
    (currentPositione3, incremente3) => currentPositione3 + incremente3,
    0
  );

  wombatPos3.subscribe(xm3 => {
    if (xm3 > 250 && xm3 < 390) {
      WOMBAT_DIV3.style.left = `${xm3}px`;
      const collision1 = makeContact(MARIO_DIV, WOMBAT_DIV1);
      const collision2 = makeContact(MARIO_DIV, WOMBAT_DIV2);
      const collision3 = makeContact(MARIO_DIV, WOMBAT_DIV3);
      const collision4 = makeContact(MARIO_DIV, WOMBAT_DIV4);
      const collision5 = makeContact(MARIO_DIV, WOMBAT_DIV5);
      const collision6 = makeContact(MARIO_DIV, WOMBAT_DIV6);

      if (collision3) {
        wombatsCollision(
          collision1,
          collision2,
          collision3,
          collision4,
          collision5,
          collision6
        );
      }
    }
  });
  // X MOVEMENT

  function randomIntFromInterval(min, max) {
    // min and max included
    return Math.floor(Math.random() * (max - min + 1) + min);
  }
})();
