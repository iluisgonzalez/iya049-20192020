(() => {
  const newStream4 = ticks4.map(() => (Math.random() <= 0.5 ? RIGHTe : LEFTe));

  // X MOVEMENT
  const leftKeyDownse4 = newStream4.filter(x4 => x4 === -1).map(() => LEFTe);
  const rightKeyDownse4 = newStream4.filter(x4 => x4 === 1).map(() => RIGHTe);

  const movementse4 = Stream.merge(leftKeyDownse4, rightKeyDownse4);

  const initialPositione4 = Stream.of(randomIntFromInterval(140, 150));
  const positionIncrementse4 = movementse4.map(
    directione4 => directione4 * VELOCITY_X_ENEMY
  );

  const wombatPos4 = Stream.merge(initialPositione4, positionIncrementse4).scan(
    (currentPositione4, incremente4) => currentPositione4 + incremente4,
    0
  );

  wombatPos4.subscribe(xm4 => {
    if (xm4 > 100 && xm4 < 190) {
      WOMBAT_DIV4.style.left = `${xm4}px`;
      const collision1 = makeContact(MARIO_DIV, WOMBAT_DIV1);
      const collision2 = makeContact(MARIO_DIV, WOMBAT_DIV2);
      const collision3 = makeContact(MARIO_DIV, WOMBAT_DIV3);
      const collision4 = makeContact(MARIO_DIV, WOMBAT_DIV4);
      const collision5 = makeContact(MARIO_DIV, WOMBAT_DIV5);
      const collision6 = makeContact(MARIO_DIV, WOMBAT_DIV6);

      if (collision4) {
        wombatsCollision(
          collision1,
          collision2,
          collision3,
          collision4,
          collision5,
          collision6
        );
      }
    }
  });
  // X MOVEMENT

  function randomIntFromInterval(min, max) {
    // min and max included
    return Math.floor(Math.random() * (max - min + 1) + min);
  }
})();
