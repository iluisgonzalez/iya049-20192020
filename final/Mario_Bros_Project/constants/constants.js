//STREAM CLASS
const { Stream } = window;
//STREAM CLASS

//DIVS FROM INDEX
const MARIO_DIV = document.querySelector(".mario");

const WOMBAT_DIV1 = document.querySelector(".wombat1");
const WOMBAT_DIV2 = document.querySelector(".wombat2");
const WOMBAT_DIV3 = document.querySelector(".wombat3");
const WOMBAT_DIV4 = document.querySelector(".wombat4");
const WOMBAT_DIV5 = document.querySelector(".wombat5");
const WOMBAT_DIV6 = document.querySelector(".wombat6");

const COIN_DIV1 = document.querySelector(".coin1");
const COIN_DIV2 = document.querySelector(".coin2");
const COIN_DIV3 = document.querySelector(".coin3");
const COIN_DIV4 = document.querySelector(".coin4");

const LIVES = document.querySelector(".gaugeLifes");
const COINS = document.querySelector(".gaugeCoin");
//DIVS FROM INDEX

//MAIN CONTROLLER
const UP = -1;
const DOWN = 1;
const LEFT = -1;
const RIGHT = 1;
const VELOCITY_PLAYER = 10;
const VELOCITYINY_PLAYER = 10;
//MAIN CONTROLLER

//ENEMIES CONTROLLERS
const LEFTe = -1; 
const RIGHTe = 1; 
const VELOCITY_X_ENEMY = 10;
//ENEMIES CONTROLLERS


//COINS TO WIN
const COINS_TO_WIN = 4;
//COINS TO WIN