function makeContact(elementCollision1, elementCollision2) {
  const rect1 = elementCollision1.getBoundingClientRect();
  const rect2 = elementCollision2.getBoundingClientRect();
  const isInHoriztonalBounds =
    rect1.x < rect2.x + rect2.width && rect1.x + rect1.width > rect2.x;
  const isInVerticalBounds =
    rect1.y < rect2.y + rect2.height && rect1.y + rect1.height > rect2.y;
  const isOverlapping = isInHoriztonalBounds && isInVerticalBounds;

  return isOverlapping;
}
